from flask import Flask
from common.data_source import AbstractDataSource

class UserService(Flask):
    key = 'user_id'
    data_keys = ('gender', 'age')

    def __init__(self, data_source: AbstractDataSource):
        super().__init__('user')
        self.init_urls()
        self._data = dict()
        data = data_source.read_data()
        for row in data:
            assert row[self.key] not in self._data, f'Key value {self.key}={row[self.key]} is not unique in self._data'
            self._data[row[self.key]] = {k: row[k] for k in self.data_keys}

    def init_urls(self):
        urls = [
            ('/users/<id>', self.get_user_data_route, {}),
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def get_user_data_route(self, id):
        return self._data.get(int(id))

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8000, **kwargs)