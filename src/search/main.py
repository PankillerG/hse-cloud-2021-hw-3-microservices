from common.data_source import CSV
from metasearch.service import MetaSearchService
from search.service import SearchInShardsService, SimpleSearchService
from settings import USER_DATA_FILE, SEARCH_DOCUMENTS_DATA_FILES, datafile
from user.service import UserService
from argparse import ArgumentParser

def main():
    parser = ArgumentParser()
    parser.add_argument('svc', type=str, choices=['user', 'metasearch', 'search'])
    parser.add_argument('--search-document-data-file', default=SEARCH_DOCUMENTS_DATA_FILES[0])
    parser.add_argument('--shards', nargs='+')
    args = parser.parse_args()

    if args.svc == 'user':
        server = UserService(CSV(USER_DATA_FILE))
    elif args.svc == 'metasearch':
        search = SearchInShardsService(shards=args.shards)
        server = MetaSearchService(search)
    elif args.svc == 'search':
        server = SimpleSearchService(CSV(datafile(args.search_document_data_file)))

    print(f"Running {args.svc}...")
    server.run_server(debug=True)

if __name__ == '__main__':
    main()