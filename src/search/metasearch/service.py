from flask import Flask, request
from typing import List, Dict
import requests

from search.service import SearchInShardsService


class MetaSearchService(Flask):
    def __init__(self, search: SearchInShardsService) -> None:
        super().__init__('metasearch')
        self.init_urls()
        self._search = search

    def get_user_data(self, user_id):
        r = requests.get(f"http://user:8000/users/{user_id}")
        res = r.json()
        print(res)
        return res

    def search(self, search_text, user_id, limit=10) -> List[Dict]:
        user_data = self.get_user_data(user_id)  # {'gender': ..., 'age': ...}
        df = self._search.get_search_data(search_text, user_data, limit)
        return df[self._search.DOCS_COLUMNS].to_dict('records')

    def init_urls(self):
        urls = [
            ('/search', self.search_route, {}),
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def search_route(self):
        text = request.args.get('text')
        user_id = int(request.args.get('user_id'))
        sr = self.search(text, user_id)
        return {'search_results': sr}

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8000, **kwargs)