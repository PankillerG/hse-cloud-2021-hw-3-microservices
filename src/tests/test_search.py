import time

import pytest
import requests

from settings import SEARCH_SERVICE_ADDRESS, SEARCH_SERVICE_PORT


@pytest.fixture
def search_baseurl():
    return f'http://{SEARCH_SERVICE_ADDRESS}:{SEARCH_SERVICE_PORT}'


@pytest.fixture
def ensure_search_running(search_baseurl):
    while True:
        try:
            response = requests.get(search_baseurl)
            assert response.status_code == 404
            break
        except requests.exceptions.ConnectionError:
            print('Waiting for search to run...')
            time.sleep(1)


@pytest.mark.usefixtures('ensure_search_running')
def test_search_service(search_baseurl):
    path = 'search'
    params = 'text=United&user_id=35'
    url = f'{search_baseurl}/{path}?{params}'
    response = requests.get(url)
    expected = {
        "search_results": [
            {
                "document": " VALKENBURG, Netherlands (Reuters) - The European Union will  draw up sanctions against Sudan, with a view to implementing  them if the United Nations calls for such measures, the Dutch  foreign minister said on Saturday.",
                "key": "EU to Draft Sanctions Against Sudan",
                "key_md5": "fff953b12941a60769cd6533a6e89f36"
            },
            {
                "document": " VALKENBURG, Netherlands (Reuters) - The European Union will  draw up sanctions against Sudan, with a view to implementing  them if the United Nations calls for such measures, the Dutch  foreign minister said on Saturday.",
                "key": "EU to Draft Sanctions Against Sudan",
                "key_md5": "fff953b12941a60769cd6533a6e89f36"
            },
            {
                "document": "AP - The U.N. Security Council voted unanimously Tuesday to hold a rare meeting in Nairobi next month to promote a peace agreement between the Sudanese government and southern rebels that the United States says also is crucial to ending the conflict in the Darfur region.",
                "key": "Security Council Votes for Nairobi Meeting (AP)",
                "key_md5": "ffe162962ad2abfd2c4fabfba82b2fb0"
            },
            {
                "document": "AP - The U.N. Security Council voted unanimously Tuesday to hold a rare meeting in Nairobi next month to promote a peace agreement between the Sudanese government and southern rebels that the United States says also is crucial to ending the conflict in the Darfur region.",
                "key": "Security Council Votes for Nairobi Meeting (AP)",
                "key_md5": "ffe162962ad2abfd2c4fabfba82b2fb0"
            },
            {
                "document": "Zenit St. Petersburg faces the daunting prospect of being picked to play against Lazio, Newcastle United or Feyenoord in Tuesday #39;s draw for the inaugural group stage of the UEFA Cup.",
                "key": "Zenit Eyes Lazio, Newcastle in Draw",
                "key_md5": "ffda40b56e51716dd0a093082e44c6b5"
            },
            {
                "document": "Zenit St. Petersburg faces the daunting prospect of being picked to play against Lazio, Newcastle United or Feyenoord in Tuesday #39;s draw for the inaugural group stage of the UEFA Cup.",
                "key": "Zenit Eyes Lazio, Newcastle in Draw",
                "key_md5": "ffda40b56e51716dd0a093082e44c6b5"
            },
            {
                "document": "Ryan Giggs will try to tie Manchester United team mate Gary Neville in knots and score a goal to send the Old Trafford crowd home in tears when Wales side play England on Saturday.",
                "key": "Giggs out to break Old Trafford hearts",
                "key_md5": "ffb166965287421f151aa614110afc16"
            },
            {
                "document": "Ryan Giggs will try to tie Manchester United team mate Gary Neville in knots and score a goal to send the Old Trafford crowd home in tears when Wales side play England on Saturday.",
                "key": "Giggs out to break Old Trafford hearts",
                "key_md5": "ffb166965287421f151aa614110afc16"
            },
            {
                "document": "There are plenty of reports on the final day of the Expos, how the city that no one in the United States felt deserved a team is finally losing it.",
                "key": "Top 10 depressing things about Expos #39; finale",
                "key_md5": "ff9923974df967da12c117876f60f293"
            },
            {
                "document": "There are plenty of reports on the final day of the Expos, how the city that no one in the United States felt deserved a team is finally losing it.",
                "key": "Top 10 depressing things about Expos #39; finale",
                "key_md5": "ff9923974df967da12c117876f60f293"
            }
        ]
    }
    assert response.json() == expected